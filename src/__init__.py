#src\__init__.py

from flask import Flask
from dotenv import load_dotenv
from .views import init_app_blueprints
from .extensions import db, bcrypt, login_manager

from .views.user.models import User

load_dotenv()

def init_db(app):
    # Bind the SQLAlchemy object to your Flask app
    db.init_app(app)
    # Check if the database and tables exist, and create them if not
    with app.app_context():
        db.create_all()
        # TODO: Remove this, dummy user
        if not User.query.get('1'):
            user = User(user_id = '1', name='John Doe', email='john@example.com', password='password')
            db.session.add(user)
            db.session.commit()


def create_app(config_file):
    app = Flask(__name__)

    #Load configurations
    app.config.from_pyfile(config_file)

    #Register Blueprints i.e initializing routes
    init_app_blueprints(app)

    # Initialize extensions 
    init_db(app)
    bcrypt.init_app(app)
    login_manager.init_app(app)

    
    return app

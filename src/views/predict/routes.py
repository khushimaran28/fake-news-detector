#  src\views\predict\routes.py

import uuid
from flask import render_template, redirect, url_for, current_app, request
from flask_login import login_required
from . import predict_bp
from .forms import PredictionInputForm
from .utils import determine_news_fake
from .models import News, get_news_result
from src.extensions import db

# TODO: Protect this route when not in session
@predict_bp.route('/', methods = ['GET', 'POST'])
@login_required
def predict():
    form = PredictionInputForm()
    if form.validate_on_submit():
        print(form.news.data)
        input_news = form.news.data

        # Send the news to model to check whether it is real or fake
        result = determine_news_fake(input_news)
        print(result)
        
        news_id = str(uuid.uuid4())
        with current_app.app_context():
            news = News(id=news_id, text=input_news, result=result, user_id='1')

            # Add the news to the database
            db.session.add(news)
            db.session.commit()

        # TODO: Should pass user_id?
        return redirect(url_for('predict.result', user_id='1', news_id=news_id))
        

    return render_template('predict/predict_view.html', form = form)

# TODO: Protect this route even in session
@predict_bp.route('/result')
@login_required
def result():
    #result = 'Dummy'
    user_id = request.args.get('user_id')
    news_id = request.args.get('news_id')

    # Retrieve the result based on user_id and news_id from the database
    result = get_news_result(user_id, news_id)

    if result is not None:
        return render_template('predict/result_view.html', result=result)

    return render_template('predict/result_view.html', result = 'NOT FOUND')


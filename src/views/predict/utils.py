import re
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from src.constants.constants import MODEL_PATH, MAXLEN


def remove_special_chars(text):
    """
    This function removes all special characters from the input text.
    It also converts the text to lowercase.
    """
    return re.sub(r'[^a-zA-Z0-9\s]', '', text.lower())


def determine_news_fake(input_news):
    """
    This function takes an input news text and determines whether it is fake or not.
    It uses a pre-trained KGPTalkie model to make the prediction.
    """
    # Load the pre-trained model
    model = load_model(MODEL_PATH)

    # Initialize the tokenizer
    tokenizer = Tokenizer()

    # Preprocess the news text
    news = remove_special_chars(input_news)

    # Tokenize and pad the sequence
    tokenizer.fit_on_texts([news])
    X = tokenizer.texts_to_sequences([news])
    X = pad_sequences(X, maxlen=MAXLEN)

    # Make the prediction
    prediction = model.predict(X)

    # Determine whether the news is fake or not
    result = "Fake" if prediction <= 0.5 else "Real"

    return result

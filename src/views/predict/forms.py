from flask_wtf import FlaskForm
from wtforms import TextAreaField, SubmitField
from wtforms.validators import DataRequired


class PredictionInputForm(FlaskForm):
    news = TextAreaField('News', validators=[DataRequired()])
    submit = SubmitField('Check News')

    def __repr__(self):
        return f"PredictionInputForm({self.news})"


#src\views\predict\models.py

from src.extensions import db
from src.views.user.models import User

# Define the News model
class News(db.Model):
    id = db.Column(db.String(64), primary_key=True)
    text = db.Column(db.Text, nullable=False)
    result = db.Column(db.String(10), nullable=False)
    user_id = db.Column(db.String(64), db.ForeignKey('user.id'), nullable=False)

    def __repr__(self):
        return f"News('{self.text}', '{self.result}')"


def get_news_result(user_id, news_id):
    # Assuming you have a 'User' model
    user = User.query.get(user_id)

    # Fetch the news result based on user_id and news_id
    news = News.query.filter_by(id=news_id, user=user).first()

    if news:
        return news.result
    else:
        return None

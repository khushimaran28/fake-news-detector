from .home import home_bp
from .predict.routes import predict_bp
from .auth.routes import auth_bp
from .user.routes import user_bp

def init_app_blueprints(app):
    app.register_blueprint(home_bp)
    app.register_blueprint(predict_bp)
    app.register_blueprint(auth_bp)
    app.register_blueprint(user_bp)
    return app

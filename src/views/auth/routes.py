#  src\views\auth\routes.py

import uuid
from flask_login import login_user, current_user, logout_user
from flask import render_template, redirect, url_for, current_app
from . import auth_bp
from .forms import RegistrationForm, LoginForm
from src.views.user.models import User
from src.extensions import bcrypt, db


@auth_bp.route('/signup', methods=['GET', 'POST'])
def signup():
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user_id = str(uuid.uuid4())
        with current_app.app_context():
            users = User(id=user_id, name=form.name.data, email=form.email.data, password=hashed_password)
            db.session.add(users)
            db.session.commit()
            return redirect(url_for('auth.login'))
    return render_template('auth/signup_view.html', form=form)


@auth_bp.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        print(user)
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            print('Check')
            login_user(user)
            return redirect(url_for('predict.predict'))
    return render_template('auth/login_view.html', form=form)

@auth_bp.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('home.home'))

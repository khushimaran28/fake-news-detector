# src\views\user\models.py

from src.extensions import db, login_manager
from flask_login import UserMixin

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)



# Define the User model
class User(db.Model, UserMixin):
    id = db.Column(db.String(64), primary_key=True) 
    name = db.Column(db.String(100), nullable=False) 
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(256), nullable=False) 
    # Define a relationship between the User and News models
    news = db.relationship('News', backref='user', lazy=True)

    def __repr__(self):
        return f"User('{self.name}', '{self.email}')"

#We also define a relationship between the `User` and `News` models. This allows us to retrieve all news articles written by a specific user. The `backref` parameter in the `db.relationship` function allows us to access the user who wrote a specific news article from the news article object. The `lazy` parameter determines how the related objects are loaded from the database. In this case, we set it to `True` to load the related objects laz

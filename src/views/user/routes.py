#  src\views\auth\routes.py

from flask import render_template
from . import user_bp

@user_bp.route('/')
def predict():
    return render_template('user_view.html')

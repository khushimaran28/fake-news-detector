from flask import render_template, url_for, redirect, request
from fakenews import app, db, bcrypt
from fakenews.forms import RegistrationForm, LoginForm
from fakenews.models import User
from flask_login import login_user, current_user, logout_user

@app.route('/home')
def home():
    return render_template('home.html')

@app.route('/login', methods = ['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user)
            return redirect(url_for('home'))
    return render_template('login.html', form = form)

@app.route('/register', methods = ['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        users = User(name=form.name.data, email=form.email.data, password=hashed_password)
        db.session.add(users)
        db.session.commit()
        return redirect(url_for('home'))
    return render_template('register.html', form = form)

@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('home'))

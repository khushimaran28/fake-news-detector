#app.py

from src import create_app
from src.constants.constants import CONFIG_FILE_PATH


#print(CONFIG_FILE_PATH)

app = create_app(CONFIG_FILE_PATH)


if __name__ == '__main__':
    app.run()
